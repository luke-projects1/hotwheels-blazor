﻿namespace Common
{
    public record MiniaturCar
    {
        public string? Brand { get; set; }
        public string Model { get; set; } = "";
        public string? Color { get; set; }
        public int Price { get; set; }
        public bool Premium { get; set; } = false;
    }
}