﻿using Common;
using System.Net.Http;
using System.Text.Json.Nodes;
using System.Text;
using static System.Runtime.InteropServices.JavaScript.JSType;
using Newtonsoft.Json;
using System;

namespace ApiServices
{
    public class HotWheelsApi
    {
        static readonly HttpClient client = new ();
        private readonly string _apiKey;
        private readonly string _apiKeySecret;
        private readonly string _url = "http://localhost:5010/";
        private readonly string _addHotWheels = "add";
        private readonly string _getAll = "all";

        public async Task AddNewHotWheelAsync(MiniaturCar miniaturCar)
        {
            var url = _url + _addHotWheels;

            try
            {
                var myContent = JsonConvert.SerializeObject(miniaturCar);
                var content = new StringContent(myContent.ToString(), Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, content);
            }
            catch (Exception e)
            {
                //throw (e);
            }
        }

        public async Task<List<MiniaturCar>?> GetHotWheels()
        {
            var url = _url + _getAll;

            client.BaseAddress = new Uri(url);
            var result = await client.GetAsync(url);
            var content = result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<List<MiniaturCar>>(content);
        }
    }
}